
#include "main_window.h"

MainWindow::MainWindow()
{
  init();
}

MainWindow::MainWindow(ProgramData *pd)
{
  init();
  setProgramData(pd);
}

void MainWindow::init()
{
  top_prog=0;
  cursor_prog=0;
  hide_past=true;
  input_string=false;
  input_time=false;
  substring="";
  time_string="";
  min_length=Time();
}

void MainWindow::setProgramData(ProgramData *pd)
{
  program_data=pd;
  all_categories=pd->getAllCategories();
  all_channels=pd->getAllChannels();
}

void MainWindow::main_loop()
{
  program_data->findFavourites();
  program_data->subset(&current_progs,current_categories,current_channels,toggle_favourites_only,hide_past,substring,min_length);

  bool loop=true;
  toggle_favourites_only=false;
  int ch;
  redraw();
  while (loop)
  {
    ch = getch();
    switch (ch)
    {
      case 'p':
        {
          PrintPrograms pp(current_progs);
          pp.main_loop();
          redraw();
        }
        break;
      case 'F':
        {
          toggle_favourites_only=!toggle_favourites_only;
          program_data->subset(&current_progs,current_categories,
                               current_channels,toggle_favourites_only,hide_past,substring,min_length);
          cursor_prog=0;
          top_prog=0;
          redraw();
        }
        break;
      case 'C':
        {
          init();
          program_data->subset(&current_progs,current_categories,
                               current_channels,toggle_favourites_only,hide_past,substring,min_length);
          redraw();
        }
        break;
      case '/':
        {
          substring="";
          input_string=true;
          redraw();
          int sch=getch();
          while (sch != '\n')
          {
            if (sch==KEY_BACKSPACE)
            {
              if (substring.length()>0)
                substring.pop_back();
            }
            else
              substring+=sch;
            redraw();
            sch=getch();
          }
          input_string=false;
          program_data->subset(&current_progs,current_categories,
                               current_channels,toggle_favourites_only,hide_past,substring,min_length);
          redraw();
        }
        break;
      case 'm':
        {
          time_string="";
          input_time=true;
          redraw();
          int sch=getch();
          while (sch != '\n')
          {
            if (sch==KEY_BACKSPACE)
            {
              if (time_string.length()>0)
                time_string.pop_back();
            }
            //else if (valid(time_string+to_string(char(sch))))
              time_string+=sch;
            if (!valid(time_string))
              time_string.pop_back();
            redraw();
            sch=getch();
          }
          input_time=false;
          min_length=to_time(time_string);
          program_data->subset(&current_progs,current_categories,
                               current_channels,toggle_favourites_only,hide_past,substring,min_length);
          redraw();

        }
        break;
      case 'h':
        {
          hide_past=!hide_past;
          program_data->subset(&current_progs,current_categories,
                               current_channels,toggle_favourites_only,hide_past,substring,min_length);
          redraw();
        }
        break;
      case 'f':
        set_favourite();
        break;
      case ' ':
      case KEY_NPAGE:
        jump_increment_list();
        break;
      case KEY_BACKSPACE:
      case KEY_PPAGE:
        jump_decrement_list();
        break;
      case KEY_UP:
        decrement_list();
        break;
      case KEY_DOWN:
        increment_list();
        break;
      case KEY_ENTER:
      case KEY_RIGHT:
      case '\r':
      case '\n':
        {
          bool fav=show_desc(current_progs[cursor_prog]);
          if (fav)
            set_favourite();
          redraw();
        }
        break;
      case 'q':
      case 'Q':
        loop=false;
        break;
      case KEY_HOME:
        jump_home();
        break;
      case KEY_END:
        jump_end();
        break;
      case 's':
        program_data->save_favourites();
        break;
      case 'c':
        {
          SubsetWindow subset_window(all_categories,all_channels,
                                     &current_categories,
                                     &current_channels);
          subset_window.main_loop();
          program_data->subset(&current_progs,current_categories,current_channels,toggle_favourites_only,hide_past,substring,min_length);
          redraw();
        }
        break;
      default:
        redraw();
        break;
    }
  }

}


void MainWindow::increment_list()
{
  dim screen_dimensions;
  getmaxyx(stdscr,screen_dimensions.y,screen_dimensions.x);
  if (cursor_prog==top_prog+screen_dimensions.y-1
       && cursor_prog<int(current_progs.size())-1)
  {
    top_prog=std::min(top_prog+screen_dimensions.y,int(current_progs.size()-1));
    cursor_prog++;
  }
  else
  {
    cursor_prog=std::min(cursor_prog+1,int(current_progs.size()-1));
  }
  redraw();

}

void MainWindow::decrement_list()
{
  dim screen_dimensions;
  getmaxyx(stdscr,screen_dimensions.y,screen_dimensions.x);
  if (cursor_prog==top_prog)
  {
    top_prog=std::max(top_prog-screen_dimensions.y,0);
  }
  if (cursor_prog>0)
    cursor_prog--;
  redraw();
}

void MainWindow::jump_increment_list()
{
  dim screen_dimensions;
  getmaxyx(stdscr,screen_dimensions.y,screen_dimensions.x);
  if (cursor_prog==top_prog+screen_dimensions.y-1
      && cursor_prog<int(current_progs.size())-1)
  {
    top_prog=std::min(top_prog+screen_dimensions.y,int(current_progs.size()-1));
  }
  cursor_prog=std::min(top_prog+screen_dimensions.y-1,int(current_progs.size()-1));
  redraw();
}

void MainWindow::jump_decrement_list()
{
  dim screen_dimensions;
  getmaxyx(stdscr,screen_dimensions.y,screen_dimensions.x);
  if (cursor_prog==top_prog)
  {
    top_prog=std::max(top_prog-screen_dimensions.y-1,0);
  }
  cursor_prog=top_prog;
  redraw();
}

void MainWindow::jump_home()
{
  cursor_prog=0;
  top_prog=0;
  redraw();
}

void MainWindow::jump_end()
{
  dim screen_dimensions;
  getmaxyx(stdscr,screen_dimensions.y,screen_dimensions.x);
  cursor_prog=int(current_progs.size()-1);
  top_prog=std::max(int(current_progs.size())-screen_dimensions.y,0);
  redraw();
}

void MainWindow::redraw()
{
  init_pair(1, COLOR_BLACK, COLOR_RED);
  dim screen_dimensions;
  getmaxyx(stdscr, screen_dimensions.y, screen_dimensions.x);
  if (cursor_prog<top_prog || cursor_prog>top_prog+screen_dimensions.y)
  {
    cursor_prog=top_prog;
  }
  clear();
  for (int c_current_progs=top_prog; 
       c_current_progs<top_prog+screen_dimensions.y-int(input_string)-int(input_time)
        && c_current_progs<int(current_progs.size());
       ++c_current_progs)
  {
    std::string output_string="";
    output_string+=current_progs[c_current_progs].getDate();
    output_string+=" ";
    output_string+=current_progs[c_current_progs].getTime();
    if (current_progs[c_current_progs].favourite())
      output_string+=" * ";
    else
      output_string+="   ";
    output_string+=current_progs[c_current_progs].getTitle();
    output_string.resize(screen_dimensions.x-15,' ');
    output_string+=current_progs[c_current_progs].getChannel();
    output_string.resize(screen_dimensions.x-1,' ');
    if (c_current_progs==cursor_prog)
    {
      attron(A_REVERSE);
      addstr(output_string.c_str());
      attroff(A_REVERSE);
    }
    else
    {
      if (current_progs[c_current_progs].favourite() && !toggle_favourites_only)
        attron(COLOR_PAIR(1));
      addstr(output_string.c_str());
      if (current_progs[c_current_progs].favourite() && !toggle_favourites_only)
        attroff(COLOR_PAIR(1));
    }
    addstr("\n");

    refresh();
  }
  if (input_string)
  {
//    mvaddstr(screen_dimensions.y-1,1,'/'+substring.c_str());
    std::string current_search="/";
    current_search+=substring;
    addstr(current_search.c_str());
  }
  else if (input_time)
  {
    std::string current_search="/";
    current_search+=time_string;
    addstr(current_search.c_str());
  }
  refresh();

}

void MainWindow::set_favourite()
{
  if (current_progs[cursor_prog].favourite())
    program_data->removeFavourite(current_progs[cursor_prog].getTitle());
  else
    program_data->addFavourite(current_progs[cursor_prog].getTitle());
  program_data->subset(&current_progs,current_categories,current_channels,toggle_favourites_only,hide_past,substring);
  redraw();
}

