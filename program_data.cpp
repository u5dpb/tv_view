#include "program_data.h"

ProgramData::ProgramData() {}

void ProgramData::read_channel_index(std::string channel_index_filename) {
  std::string line;
  std::ifstream in_ch;
  in_ch.open(channel_index_filename.c_str());
  while (std::getline(in_ch, line)) {
    std::vector<std::string> tokens = tokenize(line, ',');
    if (tokens.size() <= 2)
      channel_index[tokens[0]] = tokens[1];
    else
      omitted_channel_index[tokens[0]] = tokens[1];
  }
  in_ch.close();
}

void ProgramData::read_programs(std::string listings_file) {
  std::string title;
  std::string channel;
  Date date;
  Time start_time;
  Time stop_time;
  std::string description;
  std::string category;

  all_categories.clear();
  all_channels.clear();

  // Time_Date time_and_date;

  // programs.clear();

  tinyxml2::XMLDocument doc;
  doc.LoadFile(listings_file.c_str());
  // if (doc.LoadFile("listings.xml"))
  {
    tinyxml2::XMLNode *rootnode = doc.FirstChild(); // Element("programme");
    rootnode = rootnode->NextSiblingElement("tv");
    tinyxml2::XMLNode *programme_node = rootnode->FirstChild();
    while ((programme_node = programme_node->NextSiblingElement("programme"))) {
      tinyxml2::XMLElement *programme_element = programme_node->ToElement();
      std::string start_time_str = programme_element->Attribute("start");
      std::string stop_time_str = programme_element->Attribute("stop");
      std::string channel_str = programme_element->Attribute("channel");
      title = "Unknown";
      if (programme_node->FirstChildElement("title")->GetText() != NULL)
        title = programme_node->FirstChildElement("title")->GetText();
      description = "";
      if (programme_node->FirstChildElement("desc"))
        description = programme_node->FirstChildElement("desc")->GetText();
      else if (programme_node->FirstChildElement("sub-title") &&
               programme_node->FirstChildElement("sub-title")->GetText() !=
                   NULL)
        description = programme_node->FirstChildElement("sub-title")->GetText();
      category = "";
      if (programme_node->FirstChildElement("category"))
        category = programme_node->FirstChildElement("category")->GetText();

      date.year = atoi(start_time_str.substr(0, 4).c_str());
      date.month = atoi(start_time_str.substr(4, 2).c_str());
      date.day = atoi(start_time_str.substr(6, 2).c_str());
      start_time.hour = atoi(start_time_str.substr(8, 2).c_str());
      start_time.minute = atoi(start_time_str.substr(10, 2).c_str());
      stop_time.hour = atoi(stop_time_str.substr(8, 2).c_str());
      stop_time.minute = atoi(stop_time_str.substr(10, 2).c_str());
      //      channel=channel_index[channel_str];

      if (omitted_channel_index.find(channel_str) ==
              omitted_channel_index.end() &&
          channel_index.find(channel_str) != channel_index.end()) {
        channel = channel_index[channel_str];
        programs.insert(std::pair<Time_Date, Program>(
            Time_Date(start_time, date),
            Program(title, channel, date, start_time, stop_time, description,
                    category)));
      }
      if (category.length() > 0)
        all_categories.insert(category);
      if (channel.length() > 0)
        all_channels.insert(channel);
    }
  }
}

void ProgramData::read_favourites(std::string favourites_filename) {
  this->favourites_file = favourites_filename;
  std::ifstream in;

  in.open(favourites_filename.c_str());
  if (!in.is_open()) {
    std::cerr << "Error: Cannot open favourites in: " << favourites_filename << "\n";
    exit(-1);
  }
  std::string line;
  getline(in, line);
  while (!in.eof()) {
    favourites.insert(line);
    getline(in, line);
  }
  in.close();
  in.clear();
}

void ProgramData::findFavourites() {

  for (auto ii = programs.begin(); ii != programs.end(); ++ii) {
    if (favourites.find(ii->second.getTitle()) != favourites.end()) {
      ii->second.setFavourite(true);
    } else {
      ii->second.setFavourite(false);
    }
  }
}

std::set<std::string> ProgramData::getFavourites() { return favourites; }

/*void ProgramData::printProgs()
{
  multimap<Time_Date,Program> favourite_progs;
  for (multimap<Time_Date,Program>::iterator ii=programs.begin();
       ii!=programs.end(); ++ii)
  {
    if (favourites.find(ii->second.getTitle())!=favourites.end())
    {
      favourite_progs.insert(pair<Time_Date,Program>(ii->first,ii->second));
    }
  }
  for (multimap<Time_Date,Program>::iterator ii=favourite_progs.begin();
       ii!=favourite_progs.end(); ++ii)
  {
    for (multimap<Time_Date,Program>::iterator jj=ii; jj!=favourite_progs.end();
++jj)
    {
      if (conjoined(ii->second,jj->second))
      {
        Program new_prog=ii->second+jj->second;
        favourite_progs.insert(pair<Time_Date,Program>(ii->first,new_prog));
      }
    }
  }
  for (multimap<Time_Date,Program>::iterator ii=favourite_progs.begin();
       ii!=favourite_progs.end(); ++ii)
  {
    ii->second.printProgram();
  }
}*/
/// Try to find in the Haystack the Needle - ignore case
bool ProgramData::findString(const std::string &strHaystack,
                                  const std::string &strNeedle) {
  auto it = search(strHaystack.begin(), strHaystack.end(), strNeedle.begin(),
                   strNeedle.end(), [](char ch1, char ch2) {
                     return toupper(ch1) == toupper(ch2);
                   });
  return (it != strHaystack.end());
}

void ProgramData::subset(std::vector<Program> *c_progs,
                         const std::set<std::string> c_cats,
                         const std::set<std::string> c_chans, bool favourites,
                         bool hide_past, const std::string search_string,
                         const Time min_length) {
  time_t rawtime;
  struct tm *timeinfo;

  time(&rawtime);
  timeinfo = localtime(&rawtime);

  c_progs->clear();
  for (auto ii : programs) {
    Program p = ii.second;
    if ((!hide_past || ii.first > *timeinfo) &&
        (search_string.length() == 0 ||
         findString(p.getTitle(), search_string) ||
         findString(p.getDesc(), search_string)) &&
        (((c_cats.size() == 0 ||
           c_cats.find(p.getCategory()) != c_cats.end()) &&
          (c_chans.size() == 0 ||
           c_chans.find(p.getChannel()) != c_chans.end())) &&
         (!favourites || p.favourite())) &&
        p.getLength() > min_length)
      c_progs->push_back(p);
  }
}

void ProgramData::addFavourite(std::string fav) {
  if (favourites.find(fav) == favourites.end()) {
    favourites.insert(fav);
    findFavourites();
  }
}

void ProgramData::removeFavourite(std::string no_fav) {
  if (favourites.find(no_fav) != favourites.end()) {
    favourites.erase(no_fav);
    findFavourites();
  }
}

void ProgramData::save_favourites() {
  std::ofstream out;
  out.open(favourites_file.c_str());
  if (!out.is_open()) {
    std::cerr << "Cannot open " << favourites_file << " for writing\n";
    exit(-1);
  }
  for (std::set<std::string>::iterator ii = favourites.begin();
       ii != favourites.end(); ++ii)
    out << *ii << "\n";
  out.close();
  out.clear();
}

std::set<std::string> ProgramData::getAllCategories() { return all_categories; }

std::set<std::string> ProgramData::getAllChannels() { return all_channels; }

const size_t ProgramData::size() {
  /*size_t return_size=0;
  for (multimap<Time_Date,Program>::iterator ii=programs.begin();
       ii!=programs.end(); ++ii)
  {
    return_size++;
  }
  return return_size;*/
  std::vector<Program> return_vector;
  for (std::multimap<Time_Date, Program>::iterator ii = programs.begin();
       ii != programs.end(); ++ii) {
    return_vector.push_back(ii->second);
  }
  return return_vector.size();
  // return programs.size();
}
