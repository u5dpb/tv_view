#ifndef PRINT_PROGS_H
#define PRINT_PROGS_H

#include <vector>
#include <ncurses.h>
#include <map>

#include "program.h"
#include "definitions.h"

class PrintPrograms
{
  public:
    PrintPrograms(const std::vector<Program> program_vector);
    void redraw();
    void main_loop();

  private:
    std::vector<std::string> command_list;
    int top_line;
};

#endif

