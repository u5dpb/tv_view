// the main loop for the get record program

#include <gtkmm.h>
#include <gtkmm/main.h>
#include <glibmm.h>

#include "main_window.h"

main(int argc, char *argv[])
{

  Gtk::Main kit(argc, argv);
  MainWindow main_window;
  Gtk::Main::run(main_window);
  exit(0);
}

