// Class to hold all of the program data

#ifndef PROGRAM_DATA_H
#define PROGRAM_DATA_H

#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <vector>

#include "date.h"
#include "functions.h"
#include "program.h"
#include "time.h"
#include "time_date.h"
#include "tinyxml2.h"

class ProgramData {
private:
  std::map<std::string, std::string> channel_index;
  std::map<std::string, std::string> omitted_channel_index;
  std::set<std::string> favourites;
  std::string favourites_file;
  std::multimap<Time_Date, Program> programs;
  std::set<std::string> all_categories;
  std::set<std::string> all_channels;

  bool findString(const std::string &, const std::string &);

public:
  ProgramData();
  void read_programs(std::string);
  void read_channel_index(std::string);
  void read_favourites(std::string);
  void save_favourites();
  std::set<std::string> getFavourites();

  void subset(std::vector<Program> *, const std::set<std::string>,
              const std::set<std::string>, bool favourites = false,
              bool hide_past = true, const std::string search_string = "",
              const Time min_length = Time());

  void findFavourites();
  void addFavourite(std::string fav);
  void removeFavourite(std::string no_fav);
  // void printProgs();

  std::set<std::string> getAllCategories();
  std::set<std::string> getAllChannels();

  const size_t size();
};
#endif
