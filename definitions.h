// definitions
#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <string>
#include <set>
#include <vector>
#include <ncurses.h>

#include "program.h"

struct dim
{
	int x;
	int y;
};

/*static set<string> current_categories;
static set<string> current_channels;
static set<string> all_categories;
static set<string> all_channels;*/


inline bool show_desc(Program);
inline void show_favourites(const std::set<std::string> favourites);
/*inline void set_subset();
inline void draw_subset(const vector<bool> category_settings, const vector<bool> channel_settings);
inline void return_subset(const vector<bool> category_settings, const vector<bool> channel_settings);
static int subset_x;
static int subset_y;
static int subset_toprow;*/

bool show_desc(Program prog)
{
  clear();
  std::string output_string=prog.getTitle();
  output_string+="\n\n";
  output_string+=prog.getDate();
  output_string+=" ";
  output_string+=prog.getTime();
  output_string+="\n\n";
  output_string+=prog.getDesc();
  output_string+="\n\n";
  output_string+=prog.getChannel();
  output_string+="\n";
  addstr(output_string.c_str());
  refresh();
  int temp=getch();
  if (temp=='f')
    return true;
  return false;
}

void show_favourites(const std::set<std::string> favourites)
{
  clear();
  for (auto ii : favourites)
  {
    addstr(ii.c_str());
    addstr("\n");
  }
  refresh();
  getch();
}


/*void set_subset()
{

  vector<bool> category_settings(all_categories.size());
  vector<bool> channel_settings(all_channels.size());
  int c_ii=0;
  for (auto const ii : all_categories)
  {
    if (current_categories.find(ii)!=current_categories.end())
      category_settings[c_ii]=true;
    else
      category_settings[c_ii]=false;
    c_ii++;
  }
  c_ii=0;
  for (auto const ii : all_channels)
  {
    if (current_channels.find(ii)!=current_channels.end())
      channel_settings[c_ii]=true;
    else
      channel_settings[c_ii]=false;
    c_ii++;
  }
  subset_x=0;
  subset_y=0;
  subset_toprow=0;
  draw_subset(category_settings,channel_settings);
  bool set_subset=false;
  bool loop_subset=true;
  while (loop_subset)
  {
    int ch=getch();
    switch (ch)
    {
      case 'q':
        loop_subset=false;
        break;
      case KEY_UP:
        subset_y=max(subset_y-1,0);
        draw_subset(category_settings,channel_settings);
        break;
      case KEY_DOWN:
        subset_y=min(subset_y+1,int(subset_x==0 ? category_settings.size() : channel_settings.size()));
        draw_subset(category_settings,channel_settings);
        break;
      case KEY_LEFT:
        subset_x=max(subset_x-1,0);
        subset_y=min(subset_y,int(subset_x==0 ? category_settings.size() : channel_settings.size()));
        draw_subset(category_settings,channel_settings);
        break;
      case KEY_RIGHT:
        subset_x=min(subset_x+1,1);
        subset_y=min(subset_y,int(subset_x==0 ? category_settings.size() : channel_settings.size()));
        draw_subset(category_settings,channel_settings);
        break;
      case ' ':
        if (subset_x==0)
        {
          if (subset_y<int(category_settings.size()))
            category_settings[subset_y] = !category_settings[subset_y];
          else
          {
            return_subset(category_settings,channel_settings);
            set_subset=true;
            loop_subset=false;
          }
        }
        else
        {
          if (subset_y<int(channel_settings.size()))
            channel_settings[subset_y] = !channel_settings[subset_y];
          else
          {
            return_subset(category_settings,channel_settings);
            set_subset=true;
            loop_subset=false;
          }
        }
        draw_subset(category_settings,channel_settings);
        break;
        case 's':
          return_subset(category_settings,channel_settings);
          set_subset=true;
          loop_subset=false;
          break;
    }
  }
  if (set_subset)
  {
  }

}

void draw_subset(const vector<bool> category_settings, const vector<bool> channel_settings)
{
  dim screen_dimensions;
  getmaxyx(stdscr, screen_dimensions.y, screen_dimensions.x);
  clear();
  if (subset_y>subset_toprow+screen_dimensions.y-1)
    subset_toprow++;
  if (subset_y<subset_toprow)
    subset_toprow=subset_y;
  int row=0;
  int screen_row=0;
  for (auto ii : all_categories)
  {
    if (row>=subset_toprow)
    {
      string out_str="";
      if (category_settings[row])
        out_str+="[*]  ";
      else
        out_str+="[ ]  ";
      out_str+=ii;
      out_str.resize((screen_dimensions.x)/2-5,' ');
      out_str+="\n";
      if (subset_x==0 && subset_y==row)
        attron(A_REVERSE);
      mvaddstr(screen_row,2,out_str.c_str());
      if (subset_x==0 && subset_y==row)
        attroff(A_REVERSE);
      screen_row++;
    }
    row+=1;
  }
  row=0;
  screen_row=0;
  for (auto ii : all_channels)
  {
    if (row>=subset_toprow)
    {
      string out_str="";
      if (channel_settings[row])
        out_str+="[*]  ";
      else
        out_str+="[ ]  ";
      out_str+=ii;
      out_str.resize((screen_dimensions.x)/2-5,' ');
      out_str+="\n";
      if (subset_x==1 && subset_y==row)
        attron(A_REVERSE);
      mvaddstr(screen_row,(screen_dimensions.x)/2,out_str.c_str());
      if (subset_x==1 && subset_y==row)
        attroff(A_REVERSE);
      screen_row++;
    }
    row+=1;
  }
  refresh();
}

void return_subset(const vector<bool> category_settings, const vector<bool> channel_settings)
{
  current_categories.clear();
  current_channels.clear();

  int c_ii=0;
  for (auto const ii : all_categories)
  {
    if (category_settings[c_ii])
      current_categories.insert(ii);
    c_ii++;
  }
  c_ii=0;
  for (auto const ii : all_channels)
  {
    if (channel_settings[c_ii])
      current_channels.insert(ii);
    c_ii++;
  }

}*/
#endif
