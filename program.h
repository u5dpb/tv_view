// class to hold a programs data

#ifndef PROGRAM_H
#define PROGRAM_H

#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>

#include "time.h"
#include "date.h"
#include "time_date.h"

class Program
{
  friend bool operator<(const Program &p1, const Program &p2);
  friend bool conjoined(const Program &p1, const Program &p2);
  friend Program operator+(const Program &p1, const Program &p2);
  public:
    Program &operator=(const Program &rhs);
    Program();
    Program(std::string title, std::string channel, Date date, Time start_time, 
            Time stop_time, std::string description, std::string category);
    void printProgram();
    std::string to_str();
    std::string getTitle();
    std::string getDate();
    std::string getTime();
    std::string getChannel();
    std::string getDesc();
    std::string getCategory();
    Time getLength();
    bool favourite();
    Time_Date getTimeDate();
    void setFavourite(bool f);
  private:
      std::string title;
      std::string channel;
      Date date;
      Time start_time;
      Time stop_time;
      Time length;
      std::string description;
      bool past_midnight;
      bool fav;
      std::string category;

};

#endif
