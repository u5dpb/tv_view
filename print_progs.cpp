#include "print_progs.h"

PrintPrograms::PrintPrograms(std::vector<Program> program_vector)
{
  top_line=0;
  command_list.clear();
  std::multimap<Time_Date,Program> favourite_progs;
  for (auto ii : program_vector)
  {
    favourite_progs.insert(std::pair<Time_Date,Program>(ii.getTimeDate(),ii));
  }
  for (auto ii : favourite_progs)
  {
    command_list.push_back(ii.second.to_str());
    for (auto jj : favourite_progs)
    {

      if (conjoined(ii.second,jj.second))
      {
        Program new_prog=ii.second+jj.second;
        command_list.push_back(new_prog.to_str());
        //favourite_progs.insert(pair<Time_Date,Program>(ii->second.getTimeDate(),new_prog));
      }
    }
  }
  //for (auto ii : favourite_progs)
  //{
  //  command_list.push_back(ii.second.to_str());
  //}
}

void PrintPrograms::redraw()
{
  dim screen_dimensions;
  getmaxyx(stdscr, screen_dimensions.y, screen_dimensions.x);
  clear();
  for (int c_command_list=top_line; 
       c_command_list < int(command_list.size()) 
          && c_command_list < top_line+screen_dimensions.y-1; 
       ++c_command_list)
  {
    addstr(command_list[c_command_list].c_str());
    addstr("\n");
  }
  refresh();

}

void PrintPrograms::main_loop()
{
  dim screen_dimensions;
  getmaxyx(stdscr, screen_dimensions.y, screen_dimensions.x);
  bool loop=true;

  redraw();
  while (loop)
  {
    int ch=getch();
    switch (ch)
    {
      case 'q':
        loop=false;
        break;
      case KEY_DOWN:
      case KEY_ENTER:
        top_line=std::min(top_line+1,int(command_list.size())-1);
        break;
      case KEY_UP:
        top_line=std::max(top_line-1,0);
        break;
      case ' ':
      case KEY_NPAGE:
        top_line=std::min(top_line+screen_dimensions.y-1,int(command_list.size())-1);
        break;
      case KEY_BACKSPACE:
      case KEY_PPAGE:
        top_line=std::max(top_line-screen_dimensions.y-1,0);
        break;
    }
    redraw();
  }
}
