#include <iostream>

#include "tinyxml2.h"
#include "program.h"

using namespace tinyxml2;
using namespace std;

main()
{
  XMLDocument doc;
  doc.LoadFile("listings.xml");
  //if (doc.LoadFile("listings.xml"))
  {
    XMLNode *rootnode=doc.FirstChild(); //Element("programme");
    rootnode=rootnode->NextSiblingElement("tv");
    //XMLElement *prog_element = rootnode->ToElement();
    XMLNode *programme_node=rootnode->FirstChild();
    while (programme_node=programme_node->NextSiblingElement("programme"))
    {
      XMLElement *programme_element=programme_node->ToElement();
      string start = programme_element ->Attribute("start");
      string stop = programme_element ->Attribute("stop");
      string channel = programme_element ->Attribute("channel");
      string title = programme_node->FirstChildElement("title")->GetText();
      string desc = "";
      if (programme_node->FirstChildElement("desc"))
        desc=programme_node->FirstChildElement("desc")->GetText();
      string category = "";
      if (programme_node->FirstChildElement("category"))
        category=programme_node->FirstChildElement("category")->GetText();
      cout << title << "\n";
    }
  }
}
