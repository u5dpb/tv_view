// structure to store a date

#ifndef DATE_H
#define DATE_H

struct Date
{
  int year;
  int month;
  int day;
};

bool operator<(const Date &d1, const Date &d2);
bool operator>(const Date &d1, const Date &d2);
bool operator==(const Date &d1, const Date &d2);
bool operator!=(const Date &d1, const Date &d2);
bool previous_day(const Date &d1, const Date &d2);

#endif
