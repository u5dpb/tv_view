#include "subset_window.h"

SubsetWindow::SubsetWindow()
{
  init();
}

SubsetWindow::SubsetWindow(std::set<std::string> all_categories, 
                           std::set<std::string> all_channels,
                           std::set<std::string> *current_categories,
                           std::set<std::string> *current_channels)
{
  init();
  setAllSets(all_categories,all_channels);
  setPointers(current_categories,current_channels);
}

void SubsetWindow::init()
{
  subset_x=0;
  subset_y=0;
  subset_toprow=0;

}

void SubsetWindow::setAllSets(std::set<std::string> all_categories, 
                              std::set<std::string> all_channels)
{
  this->all_categories=all_categories;
  this->all_channels=all_channels;
  
  category_settings.resize(all_categories.size());
  channel_settings.resize(all_channels.size());
}

void SubsetWindow::setPointers(std::set<std::string> *current_categories,
                               std::set<std::string> *current_channels)
{
  this->current_categories=current_categories;
  this->current_channels=current_channels;
}

void SubsetWindow::main_loop()
{

  int c_ii=0;
  for (auto const ii : all_categories)
  {
    if (current_categories->find(ii)!=current_categories->end())
      category_settings[c_ii]=true;
    else
      category_settings[c_ii]=false;
    c_ii++;
  }
  c_ii=0;
  for (auto const ii : all_channels)
  {
    if (current_channels->find(ii)!=current_channels->end())
      channel_settings[c_ii]=true;
    else
      channel_settings[c_ii]=false;
    c_ii++;
  }
  subset_x=0;
  subset_y=0;
  subset_toprow=0;
  redraw();
  bool set_subset=false;
  bool loop_subset=true;
  while (loop_subset)
  {
    int ch=getch();
    switch (ch)
    {
      case 'c':
        fill(category_settings.begin(),category_settings.end(),false);
        fill(channel_settings.begin(),channel_settings.end(),false);
        redraw();
        break;
      case 'q':
        loop_subset=false;
        break;
      case KEY_UP:
        subset_y=std::max(subset_y-1,0);
        redraw();
        break;
      case KEY_DOWN:
        subset_y=std::min(subset_y+1,int(subset_x==0 ? category_settings.size() : channel_settings.size()));
        redraw();
        break;
      case KEY_LEFT:
        subset_x=std::max(subset_x-1,0);
        subset_y=std::min(subset_y,int(subset_x==0 ? category_settings.size() : channel_settings.size()));
        redraw();
        break;
      case KEY_RIGHT:
        subset_x=std::min(subset_x+1,1);
        subset_y=std::min(subset_y,int(subset_x==0 ? category_settings.size() : channel_settings.size()));
        redraw();
        break;
      case ' ':
      case KEY_ENTER:
      case '\r':
      case '\n':
        if (subset_x==0)
        {
          if (subset_y<int(category_settings.size()))
            category_settings[subset_y] = !category_settings[subset_y];
          else
          {
            return_subset();
            set_subset=true;
            loop_subset=false;
          }
        }
        else
        {
          if (subset_y<int(channel_settings.size()))
            channel_settings[subset_y] = !channel_settings[subset_y];
          else
          {
            return_subset();
            set_subset=true;
            loop_subset=false;
          }
        }
        redraw();
        break;
        case 's':
        case 'd':
          return_subset();
          set_subset=true;
          loop_subset=false;
          break;
    }
  }
  if (set_subset)
  {
    current_categories->clear();
    int c_ii=0;
    for (auto const ii : all_categories)
    {
      if (category_settings[c_ii])
        current_categories->insert(ii);
      c_ii++;
    }
    current_channels->clear();
    c_ii=0;
    for (auto const ii : all_channels)
    {
      if (channel_settings[c_ii])
        current_channels->insert(ii);
      c_ii++;
    }
    
  }

}

void SubsetWindow::redraw()
{
  dim screen_dimensions;
  getmaxyx(stdscr, screen_dimensions.y, screen_dimensions.x);
  clear();
  if (subset_y>subset_toprow+screen_dimensions.y-1)
    subset_toprow++;
  if (subset_y<subset_toprow)
    subset_toprow=subset_y;
  int row=0;
  int screen_row=0;
  for (auto ii : all_categories)
  {
    if (row>=subset_toprow)
    {
      std::string out_str="";
      if (category_settings[row])
        out_str+="[*]  ";
      else
        out_str+="[ ]  ";
      out_str+=ii;
      out_str.resize((screen_dimensions.x)/2-5,' ');
      out_str+="\n";
      if (subset_x==0 && subset_y==row)
        attron(A_REVERSE);
      mvaddstr(screen_row,2,out_str.c_str());
      if (subset_x==0 && subset_y==row)
        attroff(A_REVERSE);
      screen_row++;
    }
    row+=1;
  }
  if (all_categories.size() > all_channels.size())
  {
      if (subset_y==row)
        attron(A_REVERSE);
      mvaddstr(screen_row,(screen_dimensions.x)/2-5,"[Done]");
      if (subset_y==row)
        attroff(A_REVERSE);
  }
  row=0;
  screen_row=0;
  for (auto ii : all_channels)
  {
    if (row>=subset_toprow)
    {
      std::string out_str="";
      if (channel_settings[row])
        out_str+="[*]  ";
      else
        out_str+="[ ]  ";
      out_str+=ii;
      out_str.resize((screen_dimensions.x)/2-5,' ');
      out_str+="\n";
      if (subset_x==1 && subset_y==row)
        attron(A_REVERSE);
      mvaddstr(screen_row,(screen_dimensions.x)/2,out_str.c_str());
      if (subset_x==1 && subset_y==row)
        attroff(A_REVERSE);
      screen_row++;
    }
    row+=1;
  }
  if (all_categories.size() < all_channels.size())
  {
      if (subset_y==row)
        attron(A_REVERSE);
      mvaddstr(screen_row,(screen_dimensions.x)/2-5,"[Done]");
      if (subset_y==row)
        attroff(A_REVERSE);
  }
  refresh();
}

void SubsetWindow::return_subset()
{
  current_categories->clear();
  current_channels->clear();

  int c_ii=0;
  for (auto const ii : all_categories)
  {
    if (category_settings[c_ii])
      current_categories->insert(ii);
    c_ii++;
  }
  c_ii=0;
  for (auto const ii : all_channels)
  {
    if (channel_settings[c_ii])
      current_channels->insert(ii);
    c_ii++;
  }

}

