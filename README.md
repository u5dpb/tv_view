# TV Schedule Viewer

This is a simple TV schedule viewer program. It reads TV listings as written in XML format. These listings can be obtained from a variety of sources on the web. They are also attached to the UK digital broadcast feed.

# Usage

get_record (-l listings_file) (-f favourites_file) (-c channel_index_file)

An example listings file and channel index file have been included in this repository.

# Installation

Run:

make get_record

# Commands

F - toggle favourites only
