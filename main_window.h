// the main window of the TSP Solver program

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include "definitions.h"
#include "program_data.h"
#include "subset_window.h"
#include "print_progs.h"
#include "time.h"

class MainWindow
{
  public:
    MainWindow();
    MainWindow(ProgramData *);
    void setProgramData(ProgramData *);
    void main_loop();
  private:
    void init();
    std::vector<Program> current_progs;
    int top_prog;
    int cursor_prog;
    bool toggle_favourites_only;
    ProgramData *program_data;
    std::set<std::string> current_categories;
    std::set<std::string> current_channels;
    std::set<std::string> all_categories;
    std::set<std::string> all_channels;
    bool hide_past;
    std::string substring;
    bool input_string;
    bool input_time;
    std::string time_string;
    Time min_length;

    void redraw();
    void increment_list();
    void decrement_list();
    void jump_increment_list();
    void jump_decrement_list();
    void jump_end();
    void jump_home();
    void set_favourite();

};

#endif
