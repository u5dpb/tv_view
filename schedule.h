#ifndef SCHEDULE_H_
#define SCHEDULE_H_

#include <map>
#include <set>
#include <time.h>
#include <ncurses.h>

#include "time_date.h"
#include "program.h"
#include "program_data.h"
#include "definitions.h"

class Schedule
{
  public:
    Schedule();
    void main_loop();
    void redraw();
    void generate_map(ProgramData *program_data);
  private:
    map<string, int> channels;
    map<int, string> channel_names;
    map<int, map<Time_Date,Program> > schedule;
    Time_Date current_time;
    Time_Date start_time;
    Time_Date end_time;
    int top_channel;
    int cursor_channel;
    Time_Date cursor_time;
    void print_programs(const Time_Date &from, const Time_Date &to, const map<Time_Date,Program> &channel_progs, const int length, const bool &cursor_line);
    void get_next_start_time(const map<Time_Date,Program> &channel_progs);
    void get_prev_start_time(const map<Time_Date,Program> &channel_progs);
};

#endif
