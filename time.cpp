#include "time.h"

Time operator+(const Time &t1, const Time &t2)
{
  Time return_time=t1;
  return_time.hour+=t2.hour;
  return_time.minute+=t2.minute;
  if (return_time.minute>=60)
  {
    return_time.hour+=return_time.minute/60;
    return_time.minute=return_time.minute%60;
  }
  if (return_time.hour>23)
    return_time.hour=return_time.hour%24;
  return return_time;
}

Time operator-(const Time &t1, const Time &t2)
{
  Time return_time=t1;
  return_time.hour-=t2.hour;
  return_time.minute-=t2.minute;
  if (return_time.hour<0)
    return_time.hour=return_time.hour+24;
  if (return_time.minute<0 && return_time.hour>0)
  {
    return_time.hour-=(return_time.minute/60)+1;
    return_time.minute=return_time.minute+60;
  }
  return return_time;
}

bool operator<(const Time &t1, const Time &t2)
{
  if (t1.hour<t2.hour)
    return true;
  else if (t1.hour==t2.hour && t1.minute<t2.minute)
    return true;
  return false;
}

bool operator>(const Time &t1, const Time &t2)
{
  if (t1.hour>t2.hour)
    return true;
  else if (t1.hour==t2.hour && t1.minute>t2.minute)
    return true;
  return false;
}

bool operator==(const Time &t1, const Time &t2)
{
  if (t1.hour==t2.hour && t1.minute==t2.minute)
    return true;
  return false;
}

bool operator!=(const Time &t1, const Time &t2)
{
  return !(t1==t2);
}

bool valid(std::string time_string)
{
  if (time_string.length()==0)
    return true;
  if (time_string.compare(":")==0)
    return false;
  if (time_string.find(":")==std::string::npos)
  {
    std::regex e ("^-?\\d+$");
    if (std::regex_match (time_string,e))
      return true;
    else 
      return false;    
  }
  else
  {
    std::regex e ("^-?\\d+?\\:?\\d*$");
    if (std::regex_match (time_string,e))
      return true;
    else 
      return false;    
  }
}

Time to_time(std::string time_string)
{
  std::regex e ("^-?\\d+\\:?\\d+$");
  if (!regex_match (time_string,e))
    return Time();
  std::vector<std::string> tokens=tokenize(time_string,':');
  return Time(std::stoi(tokens[0]),std::stoi(tokens[1]));
}

/*using namespace std;
#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include "functions.h"
main(int argc, char *argv[])
{
  if (argc<3)
  {
bool operator<(const Time &t1, const Time &t2);
    cerr << "Usage: " << argv[0] << " <time1> <time2>\n";
    exit(-1);
  }
  vector<Time> times;
  for (int i=1; i<3; ++i)
  {
    vector<string> tokens=tokenize(argv[i],':');
    Time t;
    t.hour=atoi(tokens[0].c_str());
    t.minute=atoi(tokens[1].c_str());
    times.push_back(t);
  }
  cout << "Times: " << times[0].hour << ":" << times[0].minute <<  " and " << times[1].hour << ":" << times[1].minute << "\n";
bool operator<(const Time &t1, const Time &t2);
  cout << " Time 1 < Time 2 ? ";
  if (times[0] < times[1])
    cout << "yes\n";
  else
    cout << "no\n";
  cout << " added = " << (times[0]+times[1]).hour << ":" << (times[0]+times[1]).minute << "\n";
  cout << " subtracted = " << (times[0]-times[1]).hour << ":" << (times[0]-times[1]).minute << "\n";
bool operator>(const Time &t1, const Time &t2);
}*/
