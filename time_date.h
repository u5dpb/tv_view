#ifndef TIME_DATE_H
#define TIME_DATE_H

#include <time.h>

#include "time.h"
#include "date.h"

class Time_Date
{
  public:
  Time_Date() {};
  Time_Date(Time t, Date d)
  {
    time=t;
    date=d;
  };
  Time time;
  Date date;

  Time_Date &operator=(const Time_Date &rhs)
  {
    this->time=rhs.time;
    this->date=rhs.date;
    return *this;
  };


  friend bool operator<(const Time_Date &td1, const Time_Date &td2)
  {
    if (td1.date<td2.date)
      return true;
    if (td1.date>td2.date)
      return false;

    if (td1.time<td2.time)
      return true;
    if (td1.time>td2.time)
      return false;

    return false;

  };
  friend bool operator>(const Time_Date &td1, const struct tm &td2)
  {
    if (td1.date.year > td2.tm_year+1900)
    {
      return true;
    }
    else if (td1.date.year == td2.tm_year+1900)
    {
       if (td1.date.month > td2.tm_mon+1)
       {
         return true;
       }
       else if (td1.date.month == td2.tm_mon+1)
       {
          if (td1.date.day > td2.tm_mday)
          {
            return true;
          }
          else if (td1.date.day == td2.tm_mday)
          {
            if (td1.time.hour > td2.tm_hour)
            {
              return true;
            }
            else if (td1.time.hour == td2.tm_hour)
            {
               if (td1.time.minute >= td2.tm_min)
               {
                 return true;
               }
            }
          }
       }
    }
    return false;
  }
};

/*class Time_Date
{
  public:
  Time time;
  Date date;
  friend bool operator<(const Time_Date &td1, const Time_Date &td2)
  {
    if (td1.date<td2.date)
      return true;
    if (td1.date>td2.date)
      return false;

    if (td1.time<td2.time)
      return true;
    if (td1.time>td2.time)
      return false;

    return false;

  }

};*/
#endif
