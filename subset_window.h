#ifndef SUBSET_WINDOW_H
#define SUBSET_WINDOW_H

#include "definitions.h"

class SubsetWindow
{
  public:
    SubsetWindow();
    SubsetWindow(std::set<std::string> all_categories, 
                 std::set<std::string> all_channels, 
                 std::set<std::string> *current_categories,
                 std::set<std::string> *current_channels);
    void setAllSets(std::set<std::string> all_categories, 
                    std::set<std::string> all_channels);
    void setPointers(std::set<std::string> *current_categories,
                     std::set<std::string> *current_channels);
    void main_loop();
    void redraw();
    void return_subset();

  private:
    void init();
    
    std::vector<bool> category_settings;
    std::vector<bool> channel_settings;

    int subset_x;
    int subset_y;
    int subset_toprow;

    std::set<std::string> all_categories;
    std::set<std::string> all_channels;

    std::set<std::string> *current_categories;
    std::set<std::string> *current_channels;

};


#endif
