// data for a program

#include "program.h"

Program::Program(std::string title, std::string channel, Date date, Time start_time, 
                 Time stop_time, std::string description, std::string category)
{
  this->title=title;
  this->channel=channel;
  this->date=date;
  this->start_time=start_time;
  this->stop_time=stop_time;
  this->length=stop_time-start_time;
  this->description=description;
  this->past_midnight=(stop_time<start_time);
  this->category=category;
  fav=false;
}

Program::Program()
{
  fav=false;
}

std::string Program::getTitle()
{
  return title;
}

std::string Program::getDate()
{
  std::ostringstream ostr;
  if (date.day<10)
    ostr << "0";
  ostr << date.day << "/";
  if (date.month<10)
    ostr << "0";
  ostr << date.month;
  return ostr.str();
}

std::string Program::getTime()
{
  std::ostringstream ostr;
  if (start_time.hour<10)
    ostr << "0";
  ostr << start_time.hour << ":";
  if (start_time.minute<10)
    ostr << "0";
  ostr << start_time.minute << " ";
  return ostr.str();
}

std::string Program::getChannel()
{
  return channel;
}

std::string Program::getDesc()
{
  return description;
}

std::string Program::getCategory()
{
  return category;
}

Time Program::getLength()
{
  return length;
}

bool Program::favourite()
{
  return fav;
}

Time_Date Program::getTimeDate()
{
  Time_Date return_time_date;
  return_time_date.time=start_time;
  return_time_date.date=date;
  return return_time_date;
}

void Program::setFavourite(bool f)
{
  fav=f;
}

void Program::printProgram()
{
  std::cout << to_str();
  std::cout << "\n";

}

std::string Program::to_str()
{
  auto my_predicate = [](char c)
  {
    if (isalnum(c) || c=='_')
      return false;
    return true;
  };
  std::ostringstream sstr;
  std::string sub_title=title;
  std::string::iterator ii=sub_title.begin();
  std::transform(sub_title.begin(), sub_title.end(), sub_title.begin(), ::tolower);
  std::replace(sub_title.begin(), sub_title.end(), ' ', '_');
  sub_title.erase(std::remove_if(sub_title.begin(), sub_title.end(), my_predicate), sub_title.end());
  /*while (ii!=sub_title.end())
  {
    if ((*ii)==' ')
      *(ii++)='_';
    else if (isalnum(*ii)==0 && (*ii)!='_')
      ii=title.erase(ii);
  }*/
  if (start_time.minute<10)
    sstr << "0";
  sstr << start_time.minute << " ";
  if (start_time.hour<10)
    sstr << "0";
  sstr << start_time.hour << " ";
  if (date.day<10)
    sstr << "0";
  sstr << date.day << " ";
  if (date.month<10)
    sstr << "0";
  sstr << date.month << " * /home/danb/bin/recordtv ";
  sstr << sub_title << " \"";
  Time sub_length;
  sub_length.hour=0;
  sub_length.minute=15;
  sub_length=sub_length+length;
  if (sub_length.hour<10)
    sstr << "0";
  sstr << sub_length.hour << ":";
  if (sub_length.minute<10)
    sstr << "0";
  sstr << sub_length.minute << ":00\" \"";
  sstr << channel << "\"";
  Time stop;
  stop.hour=0;
  stop.minute=15;
  stop=stop+stop_time;
  sstr << " # ";
  if (stop.minute<10)
    sstr << "0";
  sstr << stop.minute << " ";
  if (stop.hour<10)
    sstr << "0";
  sstr << stop.hour;
  return sstr.str();
}

bool operator<(const Program &p1, const Program &p2)
{
  if (p1.date<p2.date)
    return true;
  if (p1.date>p2.date)
    return false;

  if (p1.start_time<p2.start_time)
    return true;
  if (p1.start_time>p2.start_time)
    return false;

  if (p1.channel<p2.channel)
    return true;

  return false;
}

bool conjoined(const Program &p1, const Program &p2)
{
  if ((p1.date==p2.date || (p1.past_midnight 
                               && previous_day(p1.date,p2.date)))
        && p1.stop_time==p2.start_time 
        && p1.channel.compare(p2.channel)==0)
    return true;
  return false;
}

Program operator+(const Program &p1, const Program &p2)
{
  Program new_prog;
  new_prog.title=p1.title+"_"+p2.title;
  new_prog.channel=p1.channel;
  new_prog.date=p1.date;
  new_prog.start_time=p1.start_time;
  new_prog.stop_time=p2.stop_time;
  new_prog.length=p2.stop_time-p1.start_time;
  new_prog.description=p1.description+"\n"+p2.description;
  return new_prog;
}

Program &Program::operator=(const Program &rhs)
{
  this->title=rhs.title;
  this->channel=rhs.channel;
  this->date=rhs.date;
  this->start_time=rhs.start_time;
  this->stop_time=rhs.stop_time;
  this->length=rhs.length;
  this->description=rhs.description;
  this->past_midnight=rhs.past_midnight;
  this->fav=rhs.fav;
  this->category=rhs.category;
  return *this;
}

