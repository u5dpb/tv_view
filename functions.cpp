// functions for use in programs

#include "functions.h"


std::vector<std::string> tokenize(std::string input_string, char delimiter)
{
    std::vector<std::string> tokens;
    std::string::size_type start = 0;
    std::string::size_type end = 0;
    std::string::size_type from = 0;

    while ((end = input_string.find (delimiter, from)) != std::string::npos)
    {
      if (input_string[end-1]!='\\')
      {
        std::string substring=input_string.substr (start, end-start);
        tokens.push_back (substring);
        start = end+1;
      }
      from = end + 1;
    }
    if (start==end-1)
    {
      tokens.push_back("");
    }
    else
    {
      std::string substring=input_string.substr(start, end-start);
      tokens.push_back(substring);
    }
    return tokens;
}

std::string removeChar(std::string input_string, char rem)
{
  std::string return_string=input_string;
  for (std::string::iterator ii=return_string.begin(); ii!=return_string.end(); ++ii)
  {
    if (*ii==rem)
      ii=return_string.erase(ii)-1;
  }
  return return_string;
}
