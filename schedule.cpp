#include "schedule.h"

Schedule::Schedule()
{
  time_t rawtime;
  struct tm * timeinfo;

  time ( &rawtime );
  timeinfo = localtime ( &rawtime );

  current_time.date.year=timeinfo->tm_year+1900;
  current_time.date.month=timeinfo->tm_mon+1;
  current_time.date.day=timeinfo->tm_mday;
  current_time.time.hour=timeinfo->tm_hour;
  current_time.time.minute=0;
  start_time=current_time;
  if (start_time.time.minute>=30)
    start_time.time.minute=30;
  else
    start_time.time.minute=0;
  end_time=start_time;
  end_time.add_hour();
  end_time.add_hour();
}

void Schedule::generate_map(ProgramData *program_data)
{
  schedule.clear();
  vector<Program> programs;
  set<string> dummy_set;
  program_data->subset(programs, dummy_set, dummy_set,false,false);
  int channel_no=0;
  for (const auto &prog : programs)
  {
    if (channels.find(prog.getChannel())==channels.end())
    {
      channels[prog.getChannel()]=channel_no;
      channel_names[channel_no]=prog.getChannel();
      channel_no++;
    }
    schedule[channels[prog.getChannel()]][prog.getTimeDate()]=prog;
  }
}

void Schedule::main_loop()
{
  dim screen_dimensions;
  getmaxyx(stdscr, screen_dimensions.y, screen_dimensions.x);
  top_channel=0;
  cursor_channel=0;
  cursor_time=current_time;
  bool loop=true;
  redraw();
  while (loop)
  {
    int ch=getch();
    switch (ch)
    {
      case 'q':
        loop=false;
        break;
      case KEY_UP:
        {
          if (cursor_channel>top_channel)
            cursor_channel--;
          else
          {
            top_channel=max(0,top_channel-1);
            cursor_channel=top_channel;
          }
          redraw();
        }
        break;
      case KEY_DOWN:
        {
          if (cursor_channel<top_channel+screen_dimensions.y && cursor_channel<int(schedule.size()))
          {
            cursor_channel++;
          }
          else if (cursor_channel<int(schedule.size()))
          {
            top_channel+=screen_dimensions.y;
            cursor_channel++;
          }
          redraw();
        }
        break;
      case KEY_RIGHT:
        {
          get_next_start_time(schedule[cursor_channel]);
        }
        redraw();
        break;
      case KEY_LEFT:
        {
          get_prev_start_time(schedule[cursor_channel]);
        }
        redraw();
        break;
    }
  }
}

void Schedule::redraw()
{
  end_time=start_time;
  end_time.add_hour();
  end_time.add_hour();
  int channel_length=10;
  clear();
  init_pair(1, COLOR_BLACK, COLOR_RED);
  dim screen_dimensions;
  getmaxyx(stdscr, screen_dimensions.y, screen_dimensions.x);
  int current_channel=0;
  string channel_name="Channel";
  channel_name.resize(channel_length,' ');
  channel_name+=to_string(current_time.date)+" "+to_string(current_time.time);
  channel_name.resize((screen_dimensions.x-channel_length)/2,' ');
  Time one_hour;
  one_hour.hour=1;
  one_hour.minute=0;
  channel_name+=to_string(current_time.time+one_hour);
  channel_name.resize(screen_dimensions.x-6,' ');
  channel_name+=to_string(current_time.time+one_hour+one_hour);
  addstr(channel_name.c_str());
  addstr("\n");

  for (const auto &channel_data : schedule)
  {
    if (current_channel>=top_channel && current_channel<top_channel+screen_dimensions.y)
    {
      channel_name=channel_names[channel_data.first];
      channel_name.resize(channel_length,' ');
      addstr(channel_name.c_str());
      print_programs(current_time,end_time,channel_data.second,screen_dimensions.x-channel_length,cursor_channel==current_channel);
      addstr("\n");
    }
    current_channel++;
  }
  string t="start "+to_string(start_time.time.hour)+":"+to_string(start_time.time.minute)+" end "+to_string(end_time.time.hour)+":"+to_string(end_time.time.minute)+" cursor "+to_string(cursor_time.time.hour)+":"+to_string(cursor_time.time.minute)+"\n";
  addstr(t.c_str());
  refresh();
}

void Schedule::print_programs(const Time_Date &from, const Time_Date &to, const map<Time_Date,Program> &channel_progs, const int length, const bool &cursor_line)
{
  int total_minutes=to.time.minute-from.time.minute;
  if (from.time.hour>to.time.hour)
    total_minutes+=60*(to.time.hour-from.time.hour+24);
  else
    total_minutes+=60*(to.time.hour-from.time.hour);
  string return_string="";
  vector<Program> progs;
  vector<int> minutes;
  for (const auto &p : channel_progs)
  {
    if (from<p.second.getEndTimeDate() && p.second.getTimeDate()<to)
    {
      progs.push_back(p.second);
      Time_Date p_from=from;
      Time_Date p_to=to;
      if (p_to>p.second.getEndTimeDate())
        p_to=p.second.getEndTimeDate();
      if (p_from<p.second.getTimeDate())
        p_from=p.second.getTimeDate();
      int min=p_to.time.minute-p_from.time.minute;
      if (p_from.time.hour>p_to.time.hour)
        min+=60*(p_to.time.hour-p_from.time.hour+24);
      else
        min+=60*(p_to.time.hour-p_from.time.hour);
      minutes.push_back(min);
    }
  }
  for (int i=0; i<int(progs.size()); ++i)
  {
    string prog_string="|"+progs[i].getTitle();
    //prog_string+=" "+to_string(double(minutes[i]));
    prog_string.resize(int(double(length*minutes[i])/double(total_minutes+1)),' ');
    if (cursor_line && progs[i].getTimeDate()<=cursor_time && cursor_time<progs[i].getEndTimeDate())
    {
      attron(A_REVERSE);
    }
    else
    { 
      if (progs[i].favourite())
        attron(COLOR_PAIR(1));
    }
    addstr(prog_string.c_str());
    if (cursor_line && progs[i].getTimeDate()<=cursor_time && cursor_time<progs[i].getEndTimeDate())
    {
      attroff(A_REVERSE);
    }
    else
    { 
      if (progs[i].favourite())
        attroff(COLOR_PAIR(1));
    }
    //return_string+=prog_string;
  }
  //return return_string;
}


void Schedule::get_next_start_time(const map<Time_Date,Program> &channel_progs)
{
  Time_Date next_cursor_time=cursor_time;
  auto ii=channel_progs.begin();
  while (ii!=channel_progs.end() && ii->second.getTimeDate()<next_cursor_time)
    ++ii;
  if (ii!=channel_progs.end())
  {
    next_cursor_time=ii->second.getTimeDate();
  }
  if (next_cursor_time>=end_time)
  {
    start_time=cursor_time;
    next_cursor_time=cursor_time;
  }
  cursor_time=next_cursor_time;
}

void Schedule::get_prev_start_time(const map<Time_Date,Program> &channel_progs)
{
  Time_Date next_cursor_time=cursor_time;
  auto ii=channel_progs.end();
  --ii;
  while (ii!=channel_progs.end() && ii->second.getTimeDate()>=next_cursor_time)
    --ii;
  if (ii!=channel_progs.end())
  {
    next_cursor_time=ii->second.getTimeDate();
  }
  if (next_cursor_time<start_time)
  {
    start_time.subtract_hour();
    if (next_cursor_time<start_time)
      next_cursor_time=start_time;
  }
  cursor_time=next_cursor_time;
}


