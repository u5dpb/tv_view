#include <ncurses.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <algorithm>

#include "program_data.h"
#include "main_window.h"
#include "definitions.h"


int main(int argc, char *argv[])
{
  std::string listings_file="/home/danb/guide/listings.xml";
  std::string favourites_file="/home/danb/guide/favourites";
  std::string channel_index_file="/home/danb/guide/channel_index.csv";

  for (int c_argv=1; c_argv<argc; ++c_argv)
  {
    if (argv[c_argv][0]=='-')
    {
      switch (argv[c_argv][1])
      {
        case 'l':
          listings_file=argv[++c_argv];
          break;
        case 'f':
          favourites_file=argv[++c_argv];
          break;
        case 'c':
          channel_index_file=argv[++c_argv];
          break;
      }
    }
  }

  ProgramData prog_data;
  prog_data.read_channel_index(channel_index_file);
  prog_data.read_programs(listings_file);
  prog_data.read_favourites(favourites_file);
  prog_data.findFavourites();
  initscr();
  start_color();

  init_pair(1, COLOR_BLACK, COLOR_RED);

  keypad(stdscr,true);
  clear();
	noecho();
  cbreak();
  
  MainWindow main_window(&prog_data);
  main_window.main_loop();

  endwin();
  prog_data.save_favourites();

  return 0;
}


