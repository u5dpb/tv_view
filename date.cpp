#include "date.h"


bool operator<(const Date &d1, const Date &d2)
{
  if (d1.year<d2.year)
    return true;
  else if (d1.year==d2.year && d1.month<d2.month)
    return true;
  else if (d1.year==d2.year && d1.month==d2.month && d1.day<d2.day)
    return true;
  return false;
}

bool operator>(const Date &d1, const Date &d2)
{
  if (d1.year>d2.year)
    return true;
  else if (d1.year==d2.year && d1.month>d2.month)
    return true;
  else if (d1.year==d2.year && d1.month==d2.month && d1.day>d2.day)
    return true;
  return false;
}

bool operator==(const Date &d1, const Date &d2)
{
  if (d1.year==d2.year && d1.month==d2.month && d1.day==d2.day)
    return true;
  return false;
}

bool operator!=(const Date &d1, const Date &d2)
{
  return !(d1==d2);
}
bool previous_day(const Date &d1, const Date &d2)
{ // is date d2 the day after d1?
  if (d1.year==d2.year && d1.month==d2.month && d1.day-1==d2.day)
    return true;
  if (d1.year+1==d2.year && d1.month==12 && d1.day==31 && d2.month==1 && d2.day==1)
    return true;
  return false;
}

/*using namespace std;
#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include "functions.h"
main(int argc, char *argv[])
{
  if (argc<3)
  {
bool operator<(const Time &t1, const Time &t2);
    cerr << "Usage: " << argv[0] << " <time1> <time2>\n";
    exit(-1);
  }
  vector<Time> times;
  for (int i=1; i<3; ++i)
  {
    vector<string> tokens=tokenize(argv[i],':');
    Time t;
    t.hour=atoi(tokens[0].c_str());
    t.minute=atoi(tokens[1].c_str());
    times.push_back(t);
  }
  cout << "Times: " << times[0].hour << ":" << times[0].minute <<  " and " << times[1].hour << ":" << times[1].minute << "\n";
bool operator<(const Time &t1, const Time &t2);
  cout << " Time 1 < Time 2 ? ";
  if (times[0] < times[1])
    cout << "yes\n";
  else
    cout << "no\n";
  cout << " added = " << (times[0]+times[1]).hour << ":" << (times[0]+times[1]).minute << "\n";
  cout << " subtracted = " << (times[0]-times[1]).hour << ":" << (times[0]-times[1]).minute << "\n";
bool operator>(const Time &t1, const Time &t2);
}*/
