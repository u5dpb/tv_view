// structure to store a time

#ifndef TIME_H
#define TIME_H

#include <regex>

#include "functions.h"

struct Time
{
  int hour;
  int minute;
  Time() : hour(0), minute(0) {};
  Time(int h, int m) : hour(h), minute(m) {};
//  bool valid(std::string time_std::string);
};

bool valid(std::string time_string);

Time to_time(std::string time_string);

Time operator+(const Time &t1, const Time &t2);
Time operator-(const Time &t1, const Time &t2);
bool operator<(const Time &t1, const Time &t2);
bool operator>(const Time &t1, const Time &t2);
bool operator==(const Time &t1, const Time &t2);
bool operator!=(const Time &t1, const Time &t2);

#endif
