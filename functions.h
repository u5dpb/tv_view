// functions for use in programs

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <vector>
#include <string>


std::vector<std::string> tokenize(std::string input_string, char delimiter);
std::string removeChar(std::string input_string, char rem);

#endif
