#include <ncurses.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <algorithm>

using namespace std;

namespace dim
{
	int x()
	{
		int xdim;
		int ydim;
		getmaxyx(stdscr, ydim, xdim);
		return xdim;
	}
	int y()
	{
		int xdim;
		int ydim;
		getmaxyx(stdscr, ydim, xdim);
		return ydim;
	}
}

//bool run;

int i;


void redraw()
{
  //dim screen_dimensions;
  int x,y;
  getmaxyx(stdscr, y, x);
  clear();
  char line[30];
  sprintf(line,"Run %d\nDimensions %d by %d\n",i,dim::y(),dim::x());
  ++i;
  addstr(line);
  refresh();

}

void resizeHandler(int sig)
{
  refresh();
  redraw();
}

int main()
{
  i=0;
  bool run=true;
  //signal(SIGWINCH, resizeHandler);
  //signal(KEY_RESIZE, resizeHandler);
  initscr();
  //start_color();
  //signal(SIGWINCH, resizeHandler);

  //init_pair(1, COLOR_BLACK, COLOR_RED);

  keypad(stdscr,true);
  clear();
	noecho();
  cbreak();
  
  int ch;

  redraw();
  while (run)
  {
    ch = getch();
    switch (ch)
    {
      case 'q':
      case 'Q':
        run=false;
        break;
      case KEY_RESIZE:
        redraw();
        break;
      default:
        redraw();
        break;
    }
  }
  endwin();

  return 0;
}



