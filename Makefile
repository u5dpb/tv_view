# Project: VRSP
# Makefile created by Dev-C++ 4.9.9.2

PREFIX = /usr/local
CPP  = g++
MAIN_OBJ = main.o program_data.o time.o date.o program.o functions.o tinyxml2.o main_window.o subset_window.o print_progs.o
PACKAGES= 
CFLAGS = -g -lncurses -Wall -std=c++11
LIBS = #-O6 -g `/usr/bin/pkg-config --libs gtk+-2.0 libglade-2.0 cairo libglademm-2.4 gtkmm-2.4 gthread-2.0`

INCS =   
CXXINCS =  
#CFLAGS = $(CXXINCS) -O6 -g
#CFLAGS = $(INCS) -O6 -pg
RM = rm -f

tv_view: $(MAIN_OBJ)
	$(CPP) $(MAIN_OBJ) $(CFLAGS) -o tv_view $(LIBS)

install: tv_view
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp $< $(DESTDIR)$(PREFIX)/bin/tv_view

clean:
	${RM} $(MAIN_OBJ) tv_view

main.o: main.cpp
	$(CPP) -c main.cpp -o main.o $(CFLAGS)

program_data.o: program_data.cpp
	$(CPP) -c program_data.cpp -o program_data.o $(CFLAGS)

time.o: time.cpp
	$(CPP) -c time.cpp -o time.o $(CFLAGS)

date.o: date.cpp
	$(CPP) -c date.cpp -o date.o $(CFLAGS)

program.o: program.cpp
	$(CPP) -c program.cpp -o program.o $(CFLAGS)

functions.o: functions.cpp
	$(CPP) -c functions.cpp -o functions.o $(CFLAGS)

tinyxml2.o: tinyxml2.cpp
	$(CPP) -c tinyxml2.cpp -o tinyxml2.o $(CFLAGS)

main_window.o: main_window.cpp
	$(CPP) -c main_window.cpp -o main_window.o $(CFLAGS)

subset_window.o: subset_window.cpp
	$(CPP) -c subset_window.cpp -o subset_window.o $(CFLAGS)

print_progs.o: print_progs.cpp
	$(CPP) -c print_progs.cpp -o print_progs.o $(CFLAGS)

